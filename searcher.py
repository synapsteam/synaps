import matplotlib.pyplot as plt
import numpy as np
from flask_sqlalchemy import SQLAlchemy

db = None


def plot_graph(y):
    x = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    plt.plot(x, y)
    plt.savefig('plot.png')


def distance(f1, f2):
    f1 = np.array(f1)
    f2 = np.array(f2)
    return np.sqrt(np.sum((f1 - f2) ** 2))


def connectdb(app):
    global db
    app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///features'
    db = SQLAlchemy(app)
    return db


class ImageVec(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    feature = db.Column(db.ARRAY(db.Float))
    name = db.Column(db.String)

    def __init__(self, name, feature):
        self.name = name
        self.feature = feature


def get_all_cars():
    cars = ImageVec.query.all()
    return cars


def get_closest(name):
    cars = get_all_cars()
    img_db = []
    feat_base = []
    for car in cars:
        img_db.append([car.name, car.feature])
        if name == car.name:
            feat_base = car.feature
    dists = []
    for n, feat in img_db:
        dists.append([n, distance(feat_base, feat)])
    dists = sorted(dists, key=lambda x: x[1])
    return dists[:10]
