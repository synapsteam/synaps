class Car:
    image_name = ''
    brand = ''
    model = ''
    ident = ''

    def __init__(self, name, ident, num, brand='-', model='-'):
        self.image_name = name
        self.brand = brand
        self.model = model
        self.ident = ident
        self.num = num