import searcher
from util_classes import Car
from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, static_url_path="", static_folder="")

# here goes all valueable static variables
__filename__ = 'classdb.txt'  # file to store class data
__current_list__ = []  # names of images and their features
__isCorrect__ = [False for i in range(10)]  # is checked radiobuttons


def render(comparing_img):
    """
    function to render the page
    :param comparing_img:
    :return:
    """
    global __current_list__
    image = comparing_img
    plot = '/plot.png'
    similars = []
    for i, name in (range(10), __current_list__):
        car = Car(name, ident='cb' + str(i + 1), num=i + 1)
        similars.append(car)
    __current_list__ = similars
    return render_template('index.html', pr='0.0', rr='0.0', fm='0.0',
                           pattern=image, similar=similars[1], plot=plot,
                           color='#00b200', similars=similars, comparing_img=comparing_img)


@app.route("/")
def main():
    # simply starting the server
    return render('./sampleCar.jpg')


@app.route("/", methods=['POST'])
def reload():
    print request.form
    return render('./sampleCar.jpg')


@app.route("/write", methods=['POST'])
def write():
    global __current_list__, __isCorrect__, __filename__

    class_num = int(request.form['ClassNumber'])
    for arg in request.form.keys():
        try:
            num = int(arg) - 1
            __isCorrect__[num] = True
        except:
            continue

    with open(__filename__, 'a') as file:
        for img, pred in zip(__current_list__, __isCorrect__):
            if pred:
                file.write(img.image_name + ' ' + str(class_num))
    return render('./sampleCar.jpg')


@app.route("/search", methods=['POST'])
def start_search():
    global __current_list__
    prefix = '/../../../searchProblem/'
    searcher.connectdb(app)
    image_name = request.form['img_name']
    res = searcher.get_closest(image_name)
    ds = [i[1] for i in res]
    __current_list__ = [prefix + i[0]]
    searcher.plot_graph(ds)
    image = prefix + image_name
    return render(image)


if __name__ == "__main__":
    app.run()
